<?php

namespace App\Services;

use Bitrix\Main;

class NewsService
{
    private $iblockId = 1;
    public $page = 1;
    public $page_size = 10;

    public function __construct()
    {
        define('NO_AGENT_CHECK', true);
        define('NO_KEEP_STATISTIC', true);
        define('NO_AGENT_STATISTIC', true);
        define('NOT_CHECK_PERMISSIONS', true);
        define('STOP_STATISTICS', true);
        define('PERFMON_STOP', true);
        define('SM_SAFE_MODE', true);

        require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

        \CModule::IncludeModule("iblock");
    }

    public function getList()
    {
        $res = \CIBlockElement::GetList(
            ['ACTIVE_FROM' => 'desc'],
            ['IBLOCK_ID' => $this->iblockId, 'ACTIVE' => 'Y'],
            false,
            ['nPageSize' => $this->page_size, 'iNumPage' => $this->page],
            []
        );
        while ($ob = $res->GetNext(true, false)) {
            $result['ITEMS'][] = [
                'ID' => $ob['ID'],
                'NAME' => $ob['NAME'],
                'PREVIEW_TEXT' => $ob['PREVIEW_TEXT'],
                'ACTIVE_FROM' => $ob['ACTIVE_FROM'],
                'DETAIL_PAGE_URL' => $ob['DETAIL_PAGE_URL']
            ];
        }
        return $result;
    }

    public function getById($id)
    {
        $res = \CIBlockElement::GetList(
            [],
            ['IBLOCK_ID' => $this->iblockId, 'ACTIVE' => 'Y', 'ID' => $id],
            false,
            false,
            []
        );

        if ($ob = $res->GetNext(true, false)) {
            $result['ITEMS'] = [
                'ID' => $ob['ID'],
                'NAME' => $ob['NAME'],
                'PREVIEW_TEXT' => $ob['PREVIEW_TEXT'],
                'DETAIL_TEXT' => $ob['DETAIL_TEXT'],
                'ACTIVE_FROM' => $ob['ACTIVE_FROM'],
            ];
        } else {
            $result = [];
        }

        return $result;
    }

    public function delete($id)
    {
        if (\CIBlockElement::Delete($id)) {
            $status = 'success';
        } else {
            $status = 'failed';
        }

        return $status;
    }
}
