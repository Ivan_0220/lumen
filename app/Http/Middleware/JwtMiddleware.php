<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

class JwtMiddleware
{
    /**
     * Проверка полученного токена
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->headers->get('Token');

        if (!$token) {
            return response()->json(['errors' => [['title' => 'Token not provided.']]], 401, ['Content-Type' => 'application/vnd.api+json']);
        }

        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch (ExpiredException $e) {
            return response()->json(['errors' => [['title' => 'Provided token is expired.']]], 400, ['Content-Type' => 'application/vnd.api+json']);
        } catch (Exception $e) {
            return response()->json(['errors' => [['title' => 'An error while decoding token.']]], 400, ['Content-Type' => 'application/vnd.api+json']);
        }

        //пробрасываем id пользователя для проверки на права доступа
        $request->user = $credentials->sub;

        return $next($request);
    }
}
