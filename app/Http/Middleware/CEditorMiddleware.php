<?php

namespace App\Http\Middleware;

use Closure;

class CEditorMiddleware
{
    private $iblockId = 1;

    public function __construct()
    {
        define('NO_AGENT_CHECK', true);
        define('NO_KEEP_STATISTIC', true);
        define('NO_AGENT_STATISTIC', true);
        define('NOT_CHECK_PERMISSIONS', true);
        define('STOP_STATISTICS', true);
        define('PERFMON_STOP', true);
        define('SM_SAFE_MODE', true);

        require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
    }

    /**
     * Проверим, что у пользователя есть право редактирования инфоблока новостей
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \CModule::IncludeModule("iblock");

        $iblock_permission = \CIBlock::GetPermission($this->iblockId);
        if ($iblock_permission < 'W') {
            return response()->json([
                'errors' => [['title' => $iblock_permission]]], 400, ['Content-Type' => 'application/vnd.api+json']);
        }

        return $next($request);
    }
}
