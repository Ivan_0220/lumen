<?php

namespace App\Http\Controllers;

use App\Services\NewsService;
use Validator;


class NewsController extends Controller
{
    private $news;

    public function __construct(NewsService $news)
    {
        //получение данных вынесено, т.к. контроллеру не нужно знать как получать эти данные
        $this->news = $news;
    }

    public function getAllNews($page = 1)
    {
        $result['data'] = [];

        $this->news->page = $page;
        $this->news->page_size = 10;

        $data = $this->news->getList();

        foreach ($data['ITEMS'] as $item) {
            $result['data'][] = [
                'type' => 'news',
                'id' => $item['ID'],
                'attributes' => [
                    'name' => $item['NAME'],
                    'preview_text' => $item['PREVIEW_TEXT'],
                    'active_from' => $item['ACTIVE_FROM']
                ],
                'links' => [
                    'detail' => $item['DETAIL_PAGE_URL']
                ]
            ];

            //для демонстрации возможности предоставлять ссылки для постраничной навигации
            $result['links'] = [
                'self' => '',
                'first' => '',
                'prev' => '',
                'next' => '',
                'last' => ''
            ];
        }

        return response()->json($result, 200, ['Content-Type' => 'application/vnd.api+json']);
    }

    public function getOneNews($id)
    {
        $validateFail = $this->validateNumeric($id);
        if ($validateFail) {
            return $validateFail;
        }

        $data = $this->news->getById($id);

        $result['data'] = [];

        if (!empty($data['ITEMS'])) {
            $status = 200;

            $result['data'][] = [
                'type' => 'news',
                'id' => $data['ITEMS']['ID'],
                'attributes' => [
                    'name' => $data['ITEMS']['NAME'],
                    'preview_text' => $data['ITEMS']['PREVIEW_TEXT'],
                    'detail_text' => $data['ITEMS']['DETAIL_TEXT'],
                    'active_from' => $data['ITEMS']['ACTIVE_FROM'],
                ]
            ];
        } else {
            return response()->json(['errors' => [['detail' => 'Not found']]], 404,
                ['Content-Type' => 'application/vnd.api+json']);
        }

        return response()->json($result, $status, ['Content-Type' => 'application/vnd.api+json']);
    }

    public function deleteNews($id)
    {
        $validateFail = $this->validateNumeric($id);
        if ($validateFail) {
            return $validateFail;
        }

        $res = $this->news->delete($id);
        if ($res == 'success') {
            $status = 201;
            $result['meta'] = ['title' => 'Deleted'];
        } else {
            $status = 409;
            $result['meta'] = ['title' => 'Deletion failed'];
        }

        return response()->json($result, $status, ['Content-Type' => 'application/vnd.api+json']);
    }


    private function validateNumeric($id)
    {
        $validation = Validator::make(
            ['id' => $id],
            ['id' => 'numeric']
        );

        if ($validation->fails()) {
            $errors = $validation->errors();

            $arErrors = [];
            foreach ($errors->all() as $errorValue) {
                $arErrors[] = [
                    'detail' => $errorValue
                ];
            }

            return response()->json(['errors' => $arErrors], 409, ['Content-Type' => 'application/vnd.api+json']);
        }
    }
}
