<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;

class AuthController extends Controller
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;

        define('NO_AGENT_CHECK', true);
        define('NO_KEEP_STATISTIC', true);
        define('NO_AGENT_STATISTIC', true);
        define('NOT_CHECK_PERMISSIONS', true);
        define('STOP_STATISTICS', true);
        define('PERFMON_STOP', true);
        define('SM_SAFE_MODE', true);

        require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
    }


    public function login()
    {
        //валидация логина и пароля, формирование ответа в формате jsonapi
        $validation = Validator::make($this->request->all(), [
            'login' => 'required|min:3',
            'password' => 'required'
        ]);
        if ($validation->fails()) {
            $errors = $validation->errors();

            $arErrors = [];
            foreach ($errors->all() as $errorValue) {
                $arErrors[] = [
                    'detail' => $errorValue
                ];
            }

            return response()->json(['errors' => $arErrors], 409, ['Content-Type' => 'application/vnd.api+json']);
        }

        //попытка авторизации, при успешной возвращаем токен jwt
        global $USER;
        $arAuthResult = $USER->Login($this->request->input('login'), $this->request->input('password'));

        if ($arAuthResult !== true) {
            return response()->json(['errors' => [['detail' => 'Incorrect login and password']]], 401,
                ['Content-Type' => 'application/vnd.api+json']);
        } else {
            $userId = $USER->GetID();
            $result['data'][] = [
                'type' => 'user',
                'id' => $userId,
                'attributes' => [
                    'token' => $this->jwt($userId)
                ]
            ];

            return response()->json($result, 200, ['Content-Type' => 'application/vnd.api+json']);
        }
    }

    /**
     * получаем токен
     * время жизни токена 10 минут
     * т.к. в задании не указан алгоритм, ограничимся HS256
     *
     * @param $userId
     * @return string
     */
    protected function jwt($userId)
    {
        $payload = [
            'iss' => 'jwt_test_work', // Issuer of the token
            'sub' => $userId, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60 * 10 // Expiration time
        ];

        return JWT::encode($payload, env('JWT_SECRET'), 'HS256');
    }
}
