<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//$router->get('/', function () use ($router) {
//    return $router->app->version();
//});


$router->post('/api/login/', 'AuthController@login');

$router->get('/api/news', 'NewsController@getAllNews');

//для получения определенной страницы новостей
$router->get('/api/news/page/{page}', 'NewsController@getAllNews');

//для получения одной новости
$router->get('/api/news/{id}', 'NewsController@getOneNews');

// удаление новости с проверкой авторизации и прав на редактирование
// разбил на 2 посредника для возможности повторного использования проверки авторизации при расширении функционала
// токен передается в Headers, ключ Token
$router->delete('/api/news/{id}', ['middleware'=>['jwt', 'content_editor'], 'uses'=>'NewsController@deleteNews']);
